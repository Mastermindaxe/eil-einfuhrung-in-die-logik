import java.util.Scanner;

class SatSolver {
  /**
   * @param formula die auszuwertende Formel
   * @param beta    die Belegung
   * @return        das Ergebnis der Auswertung  
   */
  static Result evaluate (String formula, boolean[] beta) throws Exception {
    if (formula.charAt(0) == 'a') {
      int primes = 0;
      while (primes+1 < formula.length() && formula.charAt(primes+1) == '\'') {
        primes ++;
      }
      return new Result(formula.substring(primes+1), beta[primes]); 
    }
    else if (formula.charAt(0) == 'w') {
      return new Result(formula.substring(1), true);
    }
    else if (formula.charAt(0) == 'f') {
      return new Result(formula.substring(1), false);
    }      
    else if (formula.charAt(0) == '~') {
      Result r = evaluate (formula.substring(1), beta);
      
      return new Result(r.rest, !r.result);
    }
    else if (formula.charAt(0) == '(') {
      Result r1 = evaluate (formula.substring(1), beta);
      char connector = r1.rest.charAt(0);
      Result r2 = evaluate (r1.rest.substring(1), beta);
      if (r2.rest.charAt(0) != ')') {
        throw new Exception ("')' expected");
      }
      
      if (connector == '&') {
        return new Result (r2.rest.substring(1), r1.result && r2.result);
      }	 	  	   	      	    	     	 	   	 	
      else if (connector == '|') {
        return new Result (r2.rest.substring(1), r1.result || r2.result);
      }
      else if (connector == '>') {
        return new Result (r2.rest.substring(1), !r1.result || r2.result);    //hier fehlt noch was
      }
      else if (connector == ':') {
        return new Result (r2.rest.substring(1), r1.result == r2.result);    //hier fehlt noch was
      }
      else {
        throw new Exception ("unknown connector");
      } 
    }
    else {
      throw new Exception ("syntax error");
    } 
  }
  
  /*
   * Die main-Methode liest im Moment eine Formel phi von der Kommandozeile und 
   * überprüft für eine Belegung  beta, ob beta ein Modell für phi ist. Um die 
   * Aufgabe zu erfüllen, muss diese Methode entsprechend angepasst werden
   */
  public static void main (String[] args) {
     boolean[] beta =  {false, true, true, true};
     Scanner sc = new Scanner(System.in);
     String phi = sc.next();
     
     try{
        System.out.println(evaluate (phi,beta).result);
     } catch(Exception e) {
        System.out.println("Syntax Error");
     }
  }
}

  class Result {	 	  	   	      	    	     	 	   	 	
      Result (String rest, boolean result) {
        this.rest = rest;
        this.result = result;
      }
      public String rest;
      public boolean result;
    }
